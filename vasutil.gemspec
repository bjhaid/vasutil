Gem::Specification.new do |s|
  s.name        = 'vasutil'
  s.version     = '0.0.1'
  s.date        = '2013-09-09'
  s.summary     = "Vasutil"
  s.description = "A gem to update subscriber RIM records from a CSV file"
  s.authors     = ["Abejide Ayodele"]
  s.email       = 'aabejide@vas-consulting.com'
  s.files       = Dir[File.dirname(__FILE__) + '/lib/*.rb']
  s.homepage    =
    'http://rubygems.org/gems/vasutil'
  s.license       = 'MIT'
end
