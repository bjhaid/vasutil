require 'subscriber_rim_service'
require 'subscriber'
require 'subscriber_service'
require 'rimprovisioning'

describe Vas::SubscriberRimService do

  after(:all) do
    puts "Deleting ./resources/rim_imsi_status.csv"
    File.delete('./resources/rim_imsi_status.csv')
  end

  before do
    @subscriber_rim_service = Vas::SubscriberRimService.new
  end
  it "load_config should be an hash" do
    expect(@subscriber_rim_service.load_config).to be_an Hash
  end

  it "load_config['rim']['username'] should be bbliteZM" do
    expect(@subscriber_rim_service.load_config["rim"]["username"]).to eq("bbliteZM")
  end

  it "splat_size should be 2 if thread size is 4 and loaded_subscribers size is 8" do
    allow(@subscriber_rim_service).to receive(:load_config).and_return({"thread_size" =>4})
    expect(@subscriber_rim_service.splat_size).to eq(2)
  end

  it "splat_size should be 10 if thread size is 10 and loaded_subscribers is 100" do
    allow(@subscriber_rim_service).to receive(:load_config).and_return({"thread_size" =>10})
    p @subscriber_rim_service.load_config["thread_size"]
    Vas::SubscriberService.any_instance.stub(:size).and_return(100)
    expect(@subscriber_rim_service.splat_size).to eq(10)
  end

  it "process should save status to file" do
    @subscriber_rim_service.loaded_subscribers.all.each do |subscriber|
      @subscriber_rim_service.process(subscriber)
      expect(File.exists? "./resources/rim_imsi_status.csv").to be_true

    end
  end

  it "process an active subscriber in the config file should have instance variable statusid == 'Active'" do
    @subscriber_rim_service.process(@subscriber_rim_service.loaded_subscribers.all.last)
    expected = @subscriber_rim_service.instance_variable_get(:@statusid)

    expect(expected).to eq("Active")
  end

  it "threaded_processing should save RIM status to file" do
    @subscriber_rim_service.threaded_processing
    expect(File.exists? "./resources/rim_imsi_status.csv").to be_true
  end

end
