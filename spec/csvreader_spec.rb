require 'csvreader'

describe Vas::CSVReader do
  file = './resources/sub.csv'
  let(:csvreader) { Vas::CSVReader.new(file) }
  it "read should return an array " do
    expect(csvreader.read).to be_an Array
  end

  it "read should an array of hash" do
    csvreader.read.each do |hash|
      expect(hash).to be_an Hash
    end
  end
end
