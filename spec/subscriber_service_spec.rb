require 'subscriber_service'
require 'subscriber'
require 'csvreader'

describe Vas::SubscriberService do

  before(:all) do
    file = './resources/sub.csv'
    data_source = Vas::CSVReader.new(file)
    @subscriber_service = Vas::SubscriberService.new(data_source)
  end

  it "all should return an array of subscriber objects" do
    @subscriber_service.all.each do |subscriber|
      puts subscriber.id
      expect(subscriber).to be_an_instance_of Vas::Subscriber
    end
  end

  context "find" do
    it "should return matching subscriber record when supplied subscriber msisdn" do
      sub = @subscriber_service.find("msisdn", '260971734407')
      sub.each do |subscriber|
        expect(subscriber.msisdn).to eq('260971734407')
      end
    end

    it "should return matching subscriber record when supplied subscriber id" do
      sub = @subscriber_service.find("id", 'CBACAE7BE8274C30E040007F010058E7')
      sub.each do |subscriber|
        expect(subscriber.id).to eq('CBACAE7BE8274C30E040007F010058E7')
      end
    end

  end
end
