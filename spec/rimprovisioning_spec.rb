require 'rimprovisioning'
require 'subscriber'

describe Vas::Rimprovisioning do

  Vas::Rimprovisioning::TRANSACTIONID = '1378427837638'
  Vas::Rimprovisioning::TIMESTAMP = "2013-09-06T02:37:17Z"

  before do
    options = {"username" => "bbliteZM","password" => "Airtelzm01", "url" => "provisioning.eu.blackberry.com"}
    @rimprovisioning = Vas::Rimprovisioning.new(options)
  end

  it "request_payload should return xml that matches specified xml" do
    @rimprovisioning.imsi = '645017701002690'
    @rimprovisioning.serviceplanid = "Prepaid Social Plan"
    expected = %Q{<?xml version='1.0'?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'><ProvisioningRequest TransactionId='1378427837638' Version='1.2' TransactionType='Modify' ProductType='BlackBerry'><Header><Sender id='101' name='WirelessCarrier'><Login>bbliteZM</Login><Password>Airtelzm01</Password></Sender><TimeStamp>2013-09-06T02:37:17Z</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'><ProvisioningDataItem name='BillingId'>645017701002690</ProvisioningDataItem><ProvisioningEntity name='service'> <ProvisioningDataItem name='ServiceName'>Prepaid Social Plan</ProvisioningDataItem></ProvisioningEntity></ProvisioningEntity></Body></ProvisioningRequest>}
    expect(@rimprovisioning.request_payload('Modify').strip).to eq(expected.strip)
  end

  it "suspend should return xml that matches specified xml" do
    @rimprovisioning.imsi = '645017701002690'
    @rimprovisioning.serviceplanid = "Prepaid Social Plan"
    expected = %Q{<?xml version='1.0'?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'><ProvisioningRequest TransactionId='1378427837638' Version='1.2' TransactionType='Modify' ProductType='BlackBerry'><Header><Sender id='101' name='WirelessCarrier'><Login>bbliteZM</Login><Password>Airtelzm01</Password></Sender><TimeStamp>2013-09-06T02:37:17Z</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'><ProvisioningDataItem name='BillingId'>645017701002690</ProvisioningDataItem><ProvisioningEntity name='service'> <ProvisioningDataItem name='ServiceName'>Prepaid Social Plan</ProvisioningDataItem></ProvisioningEntity></ProvisioningEntity></Body></ProvisioningRequest>}
    expect(@rimprovisioning.request_payload('Modify').strip).to eq(expected.strip)
  end

  it "provision should return 0 for a valid activation request" do
    @rimprovisioning.imsi = '645017722033742'
    @rimprovisioning.serviceplanid = "Prepaid Prosumer B"
    expect(@rimprovisioning.provision("Modify").to_i).to eq(0)
  end

  it "activate should return 'Success'" do
    @rimprovisioning.imsi = '645017705247642'
    @rimprovisioning.serviceplanid = "Prepaid Prosumer B"
    expect(@rimprovisioning.activate).to eq("Success")
  end

  it "initial suspend request should return 'Success'" do
    @rimprovisioning.imsi = '645017722033742'
    @rimprovisioning.serviceplanid = "Prepaid Prosumer B"
    expect(@rimprovisioning.suspend).to eq("Success")
  end

  it "subsequent suspend request should return 'Service Suspended'" do
    @rimprovisioning.imsi = '645017722033742'
    @rimprovisioning.serviceplanid = "Prepaid Prosumer B"
    @rimprovisioning.suspend
    expect(@rimprovisioning.suspend).to eq("Service Suspended")
  end

  it "suspend request for deactivated IMSI should return 'Service Inactive'" do
    @rimprovisioning.imsi = '645017722033742'
    @rimprovisioning.serviceplanid = "Prepaid Prosumer B"
    @rimprovisioning.provision('Cancel')
    expect(@rimprovisioning.suspend).to eq("Service Inactive")
  end

  it "response should be 'Success' when errorcode is '0'" do
    expect(@rimprovisioning.response('0')).to eq("Success")
  end

  it "response should be 'Service Suspended' when errorcode is '21050'" do
    expect(@rimprovisioning.response('21050')).to eq("Service Suspended")
  end

  it "response should be 'Service Suspended' when errorcode is '21040'" do
    expect(@rimprovisioning.response('21040')).to eq("Service Deactivated")
  end

  it "response should be 'Service Inactive' when errorcode is '61040'" do
    expect(@rimprovisioning.response('61040')).to eq("Service Inactive")
  end

  it "response should be 'Response: 21030 has not been mapped' when errorcode is '21030'" do
    expect(@rimprovisioning.response('21030')).to eq("Response: 21030 has not been mapped")
  end
end
