require 'csv'
module Vas
  class CSVReader
    attr_writer :file
    def initialize(file)
      @file = file
    end

    def read
      csv_data = CSV.read(@file)
      headers = csv_data.shift.map {|i| i.to_s.downcase }
      string_data = csv_data.map {|row| row.map {|cell| cell.to_s } }
      array_of_hashes = string_data.map {|row| Hash[*headers.zip(row).flatten] }
    end
  end
end
