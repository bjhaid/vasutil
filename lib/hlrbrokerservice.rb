Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }
module Vas
  class Hlrbrokerservice
    attr_accessor :subscriberservice, :csvreader, :subscriber
    def initialize(hlrdump, brokerdump, rim_parameters = {}, oracle_parameters = {})
      @oracle = Oracle.new oracle_parameters
      @rimprovisioning = Rimprovisioning.new rim_parameters
      @hlrreaddump = CSVReader.new(hlrdump)
      @brokereaddump = CSVReader.new(brokerdump)
      @subscriberservice = SubscriberService.new(@brokereaddump)
    end

    def broker_status_for_msisdn_where_hlr_imsi_differ_from_broker_imsi(msisdn, imsi)
      subs = @subscriberservice.find("msisdn",msisdn)
      @subscriber = subs.first
      if subs.empty?
        return ""
      elsif @subscriber.statusid == 'Active' && @subscriber.imsi != imsi
        return "Active"
      elsif @subscriber.statusid == 'Deactivated' && @subscriber.imsi != imsi
        return "Deactivated"
      end
    end

    def active_on_broker_with_wrong_imsi?(msisdn, imsi)
      broker_status_for_msisdn_where_hlr_imsi_differ_from_broker_imsi(msisdn, imsi) == "Active"
    end

    def deactivated_on_broker_with_wrong_imsi?(msisdn, imsi)
      broker_status_for_msisdn_where_hlr_imsi_differ_from_broker_imsi(msisdn, imsi) == "Deactivated"
    end

    def process
      @hlrreaddump.read.each do |hash|
        puts hash
        if active_on_broker_with_wrong_imsi?(hash["subscribermsisdn"], hash["subscriberimsi"])
          puts "About to update Subscriber with msisdn: #{@subscriber.msisdn} with old imsi #{@subscriber.imsi} and serviceplanid #{@subscriber.serviceplanid} with new imsi #{hash["subscriberimsi"]}"
          @oracle.update(hash["subscribermsisdn"], hash["subscriberimsi"])
          @rimprovisioning.imsi = hash["subscriberimsi"]
          @rimprovisioning.service = @subscriber.serviceplanid
          @rimprovisioning.activate
        elsif deactivated_on_broker_with_wrong_imsi?(hash["subscribermsisdn"], hash["subscriberimsi"])
          puts "About to update Subscriber with msisdn: #{@subscriber.msisdn} with old imsi #{@subscriber.imsi} and serviceplanid #{@subscriber.serviceplanid} with new imsi #{hash["subscriberimsi"]}"
          @oracle.update(hash["subscribermsisdn"], hash["subscriberimsi"])
          @rimprovisioning.imsi = hash["subscriberimsi"]
          @rimprovisioning.service = @subscriber.serviceplanid
          @rimprovisioning.suspend
        end

      end
    end


  end
end
