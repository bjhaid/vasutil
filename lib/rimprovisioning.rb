module Vas
  require 'net/http'
  require "nokogiri"
  require "open-uri"
  require 'openssl'
  class Rimprovisioning
    def initialize(options = {})
      @carrierLoginId = options["username"]
      @carrierPassword = options["password"]
      @url = options["url"]
    end
    attr_accessor :imsi, :serviceplanid, :carrierLoginId, :carrierPassword, :url
    TIMESTAMP = Time.now.strftime("%Y-%m-%dT%H:%M:%SZ")
    TRANSACTIONID= (rand(100000)/777.0).to_f.round(8).to_s.gsub(/\w+\./, "")


    def activate
      response(provision('Modify'))
    end

    def suspend
      response(provision('Suspend'))
    end

    def provision(action_type)
      rim_url = "https://#{url}/ari/submitXML"
      uri = URI(rim_url)
      http = Net::HTTP.new(uri.hostname, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      http.ssl_version = :SSLv3
      request_payload(action_type)
      @retry_count = (@retry_count.nil? || @retry_count == 0) ? 3 : @retry_count
      begin
          res = http.post(uri.path, @payload, {'Content-Type' => 'text/xml', 'Content-Length' => @payload.length.to_s, "User-Agent" => "VAS-UCIP/3.1/1.0", "Connection" => "keep-alive" })
      rescue Errno::ECONNRESET, Errno::ETIMEDOUT => e
        while retry_count > 0 do
          provision(actiion_type)
          @retry_count = retry_count - 1
        end

      end
      xml_doc  = Nokogiri::XML(res.body)
      @errorcode = xml_doc.xpath("//ErrorCode").last.children #only used for test
      res
      xml_doc.xpath("//ErrorCode").last.children.text
    end

    def request_payload(action_type)
      @payload = "<?xml version='1.0'?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'><ProvisioningRequest TransactionId='"+TRANSACTIONID+"' Version='1.2' TransactionType=\'#{action_type}\' ProductType='BlackBerry'><Header><Sender id='101' name='WirelessCarrier'><Login>#{carrierLoginId}</Login><Password>#{carrierPassword}</Password></Sender><TimeStamp>"+TIMESTAMP+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'><ProvisioningDataItem name='BillingId'>"+imsi+"</ProvisioningDataItem><ProvisioningEntity name='service'> <ProvisioningDataItem name='ServiceName'>"+serviceplanid.strip+"</ProvisioningDataItem></ProvisioningEntity></ProvisioningEntity></Body></ProvisioningRequest>"
    end

    def response errorcode
      case errorcode.to_i
      when 0
      then "Success"
      when 21040
      then "Service Deactivated"
      when 21050
      then "Service Suspended"
      when 61040
      then "Service Inactive"
      else
        "Response: #{errorcode} has not been mapped"
      end
    end
  end
end
