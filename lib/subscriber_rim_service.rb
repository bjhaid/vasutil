module Vas
Dir[File.dirname(__FILE__) + '/*.rb'].each {|file| require file }

  # require 'subscriber_service'
  # require 'rimprovisioning'
  # require 'csvreader'

  module Utility
    require 'yaml'
    def load_config
      YAML.load_file('./config/config.yml')
    end
  end

  class SubscriberRimService
    include Vas::Utility
    attr_accessor :loaded_subscribers
    def initialize
      @loaded_subscribers ||= SubscriberService.new(CSVReader.new("./resources/sub.csv"))
    end

    def splat_size
      size = @loaded_subscribers.size / load_config["thread_size"]
      (size == 0) ? 1 : size
    end

    def process(subscriber)
      rimprovisioning = Rimprovisioning.new(load_config["rim"])
      rimprovisioning.imsi = subscriber.imsi
      rimprovisioning.serviceplanid = subscriber.serviceplanid
      if (subscriber.statusid == "Active")
        File.open("./resources/rim_imsi_status.csv", "ab") { |row|  row.puts("#{subscriber.msisdn}, #{rimprovisioning.imsi}, #{rimprovisioning.activate}") }
        @statusid = "Active" #used for test purpose to confirm the if clause was reached
      else
        File.open("./resources/rim_imsi_status.csv", "ab") { |row|  row.puts("#{subscriber.msisdn},#{rimprovisioning.imsi}, #{rimprovisioning.suspend}") }
      end
    end

    def threaded_processing
      threads = []
      @loaded_subscribers.all.each_slice(splat_size) do |subscribers|
        threads << Thread.new(subscribers) do |threaded_subscribers|
          threaded_subscribers.each { |subscriber| process(subscriber) }
        end
      end
      threads.each { |thread| thread.join }
    end
  end
end
