module Vas
  class SubscriberService
    def initialize(data_source)
      @data_source = data_source
    end
    attr_accessor :data_source

    def all
      @all = @data_source.read.map do |hash|
        subscriber = Subscriber.new
        hash.each do |x,y|
          subscriber.send((x+"=" ).to_sym,y)
        end
        subscriber
      end
    end

    def find(k,v)
      all.select do |subscriber|
        subscriber.send(k.to_sym).eql? v
      end
    end

    def size
      @all ||= all
      @all.size
    end
  end

end
